Feature: End-to-End 

Scenario: Verify the functionality of Placing order by a guest with 'click and collect' facility
Given User in home page
When User selects any product name 
And enter into the product detail page
And select size and quantity
And click on add basket box
And click the mini basket icon which shows view basket and check out
And hit on proceed to checkout button
And enter the valid email as a guest on new Tu customer page
And click on continue button
And direct to click and collect button
Then should navigate to the payment details

Scenario: Verify the functionality of placing order by a guest with 'home delivery' facility

Given User is on home page
When User selects product name 
And enter into product detail page
And select the size and quantity
And click on add to basket box
And click the mini basket icon which shows view basket and the check out
And hit on proceed to checkout button
And once select the options and click continue button
And enter the valid email as guest on new Tu customer page
And click on continue button
And direct to home delivery button
And select the standard delivery or next day delivery option
And User direct to the address detail page
And enter the address details
Then should direct to the payment details

