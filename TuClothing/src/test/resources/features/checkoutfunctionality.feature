Feature: Checkout Functionality

Scenario: Verify the functionality of� click and collect� option
Given User is on checkout page
When User clicks on proceed to checkout button
And enters valid credentials
Then should direct to click and collect option
And to the payment details

Scenario: Verify the functionality of� home delivery' option'
Given User is on checkout page
When User clicks on proceed to checkout button
And enters valid credentials
Then should direct to the home delivery option
And to payment details

