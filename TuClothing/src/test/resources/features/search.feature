Feature: Search
// we just practised with scenario outline using examples and parameterized method,i did nt change any codes

Scenario Outline: Verify search with valid product name
Given User in home page
When User search for "<keyword>" keyword
Then should appear similar results

Examples:
|keyword|
|Hair Accessories|
|Menswear|
Scenario: Verify search with invalid product

Given User in home page
When User search for invalid product
Then should appear dissimilar results

Scenario: Verify search with blank content

Given User in home page
When User search for a blank product
Then should appear a warning message

Scenario: Verify search with categories

Given User in home page
When User search with categories
Then should appear right category page

Scenario: Verify functionality of search box 

Given User in home page
When User click on search bar
Then search box should highlight 

Scenario: Verify functionality of search bar

Given User in home page
When User click on particular search bar
Then search bar should highlight in black colour


