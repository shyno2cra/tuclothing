Feature: Basket Functionality

Scenario: Verify the functionality of viewing basket and adding product to the basket
Given User is on basket page
When User clicks on the view basket and check out button
Then should direct to the checkout page

Scenario: Verify the functionality of continue shopping
Given User is on basket page
When User clicks on view basket and check out button
Then should direct to the checkout page
And User should able to do continue with shopping

Scenario: Verify the functionality of ' add promotional code /pay with voucher' option
Given User is on basket page
When User clicks on view basket and the checkout button
Then User should able to click on the code or voucher option

Scenario:  Verify the functionality of 'free click and collect'
Given User is on basket page
When User clicks on free click and collect
Then User should get the delivery information update




