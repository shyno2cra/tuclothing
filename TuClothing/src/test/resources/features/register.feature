Feature: Register

Scenario: Verify the functionality of� password �and �confirm password� 
Given User is on registration page
When User enter all valid information
And enter password and confirm password with the same characters
Then should show assigned message

Scenario: Verify adding nectar points during registration process

Given User is on registration page
When User enter the nectar points while registering
Then should add on the account

Scenario: Verify User registering with all valid mandatory information
Given User is on registration page
When User enter all mandatory informations
|email|jikkukalu@gmail.com|
|firstname|shyno|
|lastname|jithosh|
|password|j11223344|
|confirmpassword|j11223344|
Then should appear as registration successful
And should direct to the home page

Scenario: Verify User without entering one of the steps for registration
Given User is on registration page
When user not enter one of the mandatory steps
Then a warning message should come

Scenario: Verify the functionality of not clicking recaptcha
Given User is on registration page
When User enter all valid information
And not clicking recaptcha
Then a warning message should appear

Scenario: Verify the function of terms and condition policy
Given User is on registration page
When User enter all valid information
 And without clicking the terms and conditions policy
 Then User should see an error message pop up.






