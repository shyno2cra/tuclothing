Feature: Store Locator

Scenario: Verify the functionality with a valid post code
Given User is on store locator page
When User enter valid post code
Then should display the nearest stores

Scenario: Verify the functionality of store locator results with a valid post code
Given User is on store locator page
When User enter with valid post code
Then should show opening hours, click and collect facility of nearest stores
#And options for stores that stock clothing

Scenario: Verify the functionality of clicking the option' where I can collect my online order' and 'stores that stock clothing'
Given User is on store locator page
When User enter a valid post code 
Then User will only be able to see the stores which is available for stock clothing and click and collect

Scenario: Verify the functionality with invalid post code
Given User is on store locator page
When User enter an invalid post code
Then should show a warning message

Scenario: Verify the functionality of store locator without entering valid post code
Given User is on store locator page
When User not entering valid post code
And click find stores
Then a warning message will appear

Scenario: Verify the functionality of without clicking the option' where I can collect my online order' and 'stores that stock clothing'
Given User is on store locator page
When User enter a valid post code 
And not clicking the given options
Then it should show all nearby stores





