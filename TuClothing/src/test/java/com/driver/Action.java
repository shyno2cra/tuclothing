package com.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

public class Action extends BaseClass {
	public void updateElement(By elementName,String keyword) {
		driver.findElement(elementName).clear();
		driver.findElement(elementName).sendKeys(keyword);
	}
public void clickOnTheElement(By elementName) {
	 driver.findElement(elementName).click();
}
public void dropDownByValue(By elementName, String value) {
	Select titleDropDown = new Select(driver.findElement(elementName));
	titleDropDown.selectByValue(value);
}
public void dropDownByIndex(By elementName, int index) {
	Select titleDropDown = new Select(driver.findElement(elementName));
	titleDropDown.selectByIndex(index);
}
public void clickOnTheElementByIndex(By elementName,int index) {
	driver.findElements(elementName).get(index).click();
}
public void dropDownByIndex1(By elementName,int index) {
	Select sizeDropdown=new Select(driver.findElement(elementName));
	sizeDropdown.selectByIndex(index);
}
public void dropDownByValue1(By elementName,String value) {
	Select quantityDropdown=new Select(driver.findElement(elementName));
	quantityDropdown.selectByValue(value);
}
public void selectCheckSizeIndex(By elementName,int index) {
	driver.findElements(elementName).get(index).click();
}
}
