package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

public class DeliveryPage extends BaseClass {
	private static By CLICKANDCOLLECTBYINDEX=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUEBUTTON=By.cssSelector(".ln-c-button.ln-c-button--primary");
	private static By LOOKUPPOSTCODE=By.cssSelector("#lookup");
	private static By LOOKUPSUBMIT=By.cssSelector("button[data-testid='lookup-submit']");
	private static By SELECTSTORE=By.cssSelector("button[data-testid='select-store']");
	private static By PROCEEDTOSUMMARY=By.cssSelector(".ln-c-button--primary");
	private static By CONTINUETOPAYMENT=By.cssSelector("button[data-testid='continueToPayment']");
	private static By PAYWITHCARD=By.linkText("Pay with a card");
	private static By TITLEDROPDOWN=By.cssSelector("#newTitle");
	private static By FIRSTNAME=By.cssSelector("#newFirstName");
	private static By SURNAME=By.cssSelector("#newSurname");
	private static By CONTACTNUMBER=By.cssSelector("#newContactNumber");
	private static By ADDRESSPOSTCODE=By.cssSelector("#addressPostcode");
	private static By FINDADDRESSBUTTON=By.cssSelector("button[data-testid='findAddress']");
	private static By ADDRESSLISTDROPDOWN=By.cssSelector("#addressListView");
	private static By TICKTOCONTACT=By.cssSelector(".ln-c-form-option__label");
	private static By TERMANDCONDITION=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUETOPAYMENT1=By.cssSelector("#contPayment");
	private static By HOMEDELIVERYBYINDEX=By.cssSelector(".ln-c-form-option__label");
	private static By TITLEDROPDOWN1=By.cssSelector("select[name='titleCode']");
	private static By FIRSTNAME1=By.cssSelector("input[name='firstName']");
	private static By LASTNAME=By.cssSelector("input[name='lastName");
	private static By FINDADDRESSBUTTON1=By.cssSelector(".address-lookup.ln-u-push-bottom");
	private static By ADDRESSLISTDROPDOWN1=By.cssSelector("select[id='addressListView']");
	private static By BILLINGADDRESS=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUEBUTTON1=By.cssSelector("#continue");
	private static By STANDARDDELIVERYBUTTON=By.cssSelector("label[data-testid='deliveryMethod']");
	private static By CONTINUEBUTTON2=By.cssSelector("input[data-testid='continue']");
	private static By ASSERTDELIVERYOPTIONS=By.cssSelector(".checkout-step-title");
	private static By ASSERTSTAYTOUCH=By.cssSelector(".checkout-step-subtitle");
	
	public void shouldDirectToClickAndCollectOption() throws InterruptedException {
		action.clickOnTheElementByIndex(CLICKANDCOLLECTBYINDEX, 0);
		Assert.assertEquals("Delivery options", get.getElementText(ASSERTDELIVERYOPTIONS));
		action.clickOnTheElement(CONTINUEBUTTON);
		Thread.sleep(3000);
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/pickup-location/findStore", get.getCurrentUrl());
		action.updateElement(LOOKUPPOSTCODE, "MK430PG");
		action.clickOnTheElement(LOOKUPSUBMIT);
		Thread.sleep(3000);
		action.clickOnTheElementByIndex(SELECTSTORE, 0);
		action.clickOnTheElement(PROCEEDTOSUMMARY);
	    Thread.sleep(3000);
	    action.clickOnTheElement(CONTINUETOPAYMENT);
		Thread.sleep(3000);
		action.clickOnTheElement(PAYWITHCARD);
		action.dropDownByValue(TITLEDROPDOWN, "mrs");
		//Select titleDropDown=new Select(driver.findElement(TITLEDROPDOWN));
		//titleDropDown.selectByValue("mrs");
		action.updateElement(FIRSTNAME, "Shyno");
		action.updateElement(SURNAME, "Jithosh");
		action.updateElement(CONTACTNUMBER, "07417513636");
		action.updateElement(ADDRESSPOSTCODE, "Mk430PG");
		action.clickOnTheElement(FINDADDRESSBUTTON);
		Thread.sleep(3000);
		action.dropDownByValue(ADDRESSLISTDROPDOWN, "5");
		//Select addressDropDown=new Select(driver.findElement(ADDRESSLISTDROPDOWN));
		//addressDropDown.selectByValue("5");
		Thread.sleep(3000);
		Assert.assertEquals("Staying in touch", get.getElementText(ASSERTSTAYTOUCH));
		action.clickOnTheElementByIndex(TICKTOCONTACT, 0);
		action.clickOnTheElementByIndex(TERMANDCONDITION, 1);
		}
	
	//home delivery
	public void shouldDirectToTheHomeDeliveryPage() throws InterruptedException {
		action.clickOnTheElementByIndex(HOMEDELIVERYBYINDEX, 1);
		Assert.assertEquals("Delivery options", get.getElementText(ASSERTDELIVERYOPTIONS));
		action.clickOnTheElement(CONTINUEBUTTON);
		Thread.sleep(3000);
		action.dropDownByIndex(TITLEDROPDOWN1, 3);
		//Select titleDropDown=new Select(driver.findElement(TITLEDROPDOWN1));
		//titleDropDown.selectByIndex(3);
		Thread.sleep(3000);
		action.updateElement(FIRSTNAME1, "Shyno");
		action.updateElement(LASTNAME, "Jithosh");
		action.updateElement(ADDRESSPOSTCODE, "MK430PG");
		action.clickOnTheElement(FINDADDRESSBUTTON1);
		Thread.sleep(3000);
		action.dropDownByIndex(ADDRESSLISTDROPDOWN1, 5);
		//Select addressDropDown=new Select(driver.findElement(ADDRESSLISTDROPDOWN1));
		//addressDropDown.selectByIndex(5);
		action.clickOnTheElement(BILLINGADDRESS);
		action.clickOnTheElement(CONTINUEBUTTON1);
		Thread.sleep(3000);
		action.clickOnTheElement(STANDARDDELIVERYBUTTON);
		action.clickOnTheElement(CONTINUEBUTTON2);
		Thread.sleep(3000);
		action.clickOnTheElement(CONTINUETOPAYMENT);
		Thread.sleep(3000);
		action.clickOnTheElement(PAYWITHCARD);
		Thread.sleep(2000);
		action.clickOnTheElement(FINDADDRESSBUTTON);
		Thread.sleep(3000);
		Assert.assertEquals("Staying in touch", get.getElementText(ASSERTSTAYTOUCH));
		action.clickOnTheElementByIndex(TICKTOCONTACT, 0);
		action.clickOnTheElementByIndex(TERMANDCONDITION, 1);
	}

}
