package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

public class BasketPage extends BaseClass {
//	BasketFunctionality
	private static By BASKET=By.linkText("TUBrands");
	private static By BASKETFUNCTIONALITYBRAND=By.linkText("Brands");
	private static By BASKETFUNCTIONALITYJEWLRYWATCHES=By.linkText("JEWELLERY & WATCHES");
	private static By BASKETFUNCTIONALITYONESIZE=By.cssSelector("#select-size");
	private static By BASKETFUNCTIONALITYDROPQUANTITY=By.cssSelector("#productVariantQty");
	private static By BASKETFUNCTIONALITYMINI=By.cssSelector("#nav_cart");
	private static By BASKETFUNCTIONALITYUNIFORM=By.linkText("School Uniform");
	private static By BASKETFUNCTIONALITYCARDIGAN=By.linkText("Cardigans");
	private static By BASKETFUNCTIONALITYCOLOUR=By.cssSelector(".tu-pdp__information-selector-link");
	private static By BASKETFUNCTIONALITYDROPSIZE=By.cssSelector("#select-size");
	private static By BASKETFUNCTIONALITYSOCKS=By.cssSelector("img['Grey Knee High Sock 5 Pack (6 Infant-5.5 Adult)']");
	private static By BASKETFUNCTIONALITYLOUNGE=By.linkText("Loungewear");
   private static By BASKETFUNCTIONALITYSLIPPERIMAGE=By.cssSelector("img[alt='Pink Stripy Open-Toe Cupsole Slippers']");
	private static By BASKETFUNCTIONALITYCHECKSIZE=By.cssSelector(".selectable");
	private static By VIEWBASKETCHECKOUTBUTTON=By.cssSelector(".ln-c-button.ln-c-button--primary");
	private static By CONTINUESHOPPING=By.cssSelector(".ln-c-button.ln-c-button--secondary.left");
	private static By PROMOTIONALCODE=By.cssSelector(".basketTotalsAddPromo.clearfix #showPromo");
	private static By BASKETFUNCTIONALITYBABY=By.linkText("Baby");
	private static By BASKETFUNCTIONALITYACCESSORIES=By.linkText("Accessories");
	private static By BASKETFUNCTIONALITYCLICKANDCOLLECT=By.linkText("Click & Collect");
	private static By BASKETFUNCTIONALITYRETURNTOSTORES=By.cssSelector("a[href='/help/returnsAndRefunds']");
	private static By MINIBASKETICON=By.cssSelector(".icon");
	private static By REMOVEITEM=By.cssSelector("#RemoveProduct_0");
	private static By ADDTOBASKETPRODUCTIMAGE=By.cssSelector(".image-component img");
	private static By ADDTOBASKETBUTTON=By.cssSelector("#AddToCart");
	private static String baseURL="https://tuclothing.sainsburys.co.uk/";
	private static By ASSERTSIZE=By.cssSelector(".ln-u-visually-hidden");
	private static By VIEWBASKETBUTTON=By.cssSelector("#basket-title");
	private static By ASSERTPRODUCTHEADLINE=By.cssSelector(".productHeadline");
	private static By ASSERTPRODUCTQUANTITY=By.cssSelector(".prod_quantity");
	private static By ASSERTH1NAME=By.cssSelector("h1");
	private static By ASSERTTHNAME=By.cssSelector("th");
	
	public void userIsOnBasketPage() throws InterruptedException {
	    action.clickOnTheElement(BASKETFUNCTIONALITYBRAND);
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/brands?INITD=GNav-Brands-", get.getCurrentUrl());
		action.clickOnTheElement(BASKETFUNCTIONALITYJEWLRYWATCHES);
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/brands/jewellery-and-watches-brands?INITD=brands_ls_jewellerywatches", get.getCurrentUrl());
		Thread.sleep(3000);
		action.clickOnTheElementByIndex(ADDTOBASKETPRODUCTIMAGE, 0);
		
		Assert.assertEquals("Size:", get.getElementText(ASSERTSIZE));
		action.clickOnTheElement(BASKETFUNCTIONALITYONESIZE);
		Thread.sleep(3000);
		action.dropDownByIndex1(BASKETFUNCTIONALITYDROPQUANTITY, 1);
//		Select quantityDropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPQUANTITY));
//		quantityDropDown.selectByIndex(1);
		Thread.sleep(3000);
	}
		public void userClicksOnViewBasketAndCheckoutButton() throws InterruptedException {
			action.clickOnTheElement(ADDTOBASKETBUTTON);
			action.clickOnTheElement(BASKETFUNCTIONALITYMINI);
		   Thread.sleep(3000);
		}
		public void shouldDirectToCheckoutPage() {
			Assert.assertEquals("Qty:2", get.getElementText(ASSERTPRODUCTQUANTITY));
		}
//		//continue shopping
//		driver.findElement(BASKETFUNCTIONALITYLOUNGE).click();
//		Thread.sleep(3000);
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/loungewear/loungewear?INITD=dd_loungewear", driver.getCurrentUrl());
//		Thread.sleep(3000);
//		System.out.println("podii");
//		driver.findElements(ADDTOBASKETPRODUCTIMAGE).get(5).click();
//		Assert.assertEquals("Size:", driver.findElement(By.cssSelector(".ln-u-visually-hidden")).getText());
//		Thread.sleep(3000);
//		Select size1DropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPSIZE));
//		size1DropDown.selectByIndex(3);
//		Thread.sleep(3000);
//		Select quantity1DropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPQUANTITY));
//		quantity1DropDown.selectByIndex(1);
//		Thread.sleep(3000);
//		//promotional code:done with above code
//		//free click and collect
//		driver.findElement(BASKETFUNCTIONALITYBABY).click();
//		Thread.sleep(3000);
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby?INITD=GNav-Baby-Header", driver.getCurrentUrl());
//		driver.findElement(BASKETFUNCTIONALITYACCESSORIES).click();
//		Thread.sleep(3000);
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby-boy-accessories?INITD=GNav-BBW-BoysAccs", driver.getCurrentUrl());
//		Thread.sleep(3000);
//		driver.findElements(ADDTOBASKETPRODUCTIMAGE).get(3).click();
//		Thread.sleep(3000);
//		Assert.assertEquals("Product Details", driver.findElement(By.cssSelector("#tu-product-details")).getText());
//		Thread.sleep(3000);
//		Select sizeDropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPSIZE));
//		sizeDropDown.selectByIndex(3);
//		Thread.sleep(3000);
//		Select quantity2DropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPQUANTITY));
//		quantity2DropDown.selectByValue("2");
//		Thread.sleep(3000);
//		driver.findElement(ADDTOBASKETBUTTON).click();
//		Thread.sleep(3000);
//		driver.findElement(BASKETFUNCTIONALITYMINI).click();
 //   driver.findElement(VIEWBASKETCHECKOUTBUTTON).click();
//		Thread.sleep(3000);
	//	Assert.assertEquals("Qty:2", driver.findElement(By.cssSelector(".prod_quantity")).getText());
		
		public void userClicksViewBasketCheckoutButton() throws InterruptedException {
			action.clickOnTheElement(ADDTOBASKETBUTTON);
			Thread.sleep(3000);
			action.clickOnTheElement(BASKETFUNCTIONALITYMINI);
			action.clickOnTheElement(VIEWBASKETCHECKOUTBUTTON);
			Thread.sleep(3000);
			Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", get.getCurrentUrl());
		}
		public void userShouldAbleToContinueShopping() throws InterruptedException {
			Thread.sleep(3000);
			action.clickOnTheElement(CONTINUESHOPPING);
			Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
			}
		public void userClicksOnViewBasketAndTheCheckoutButton() throws InterruptedException {
			action.clickOnTheElement(ADDTOBASKETBUTTON);
		   Thread.sleep(3000);
		   action.clickOnTheElement(BASKETFUNCTIONALITYMINI);
		   Thread.sleep(3000);
		   action.clickOnTheElement(VIEWBASKETCHECKOUTBUTTON);
		   Assert.assertEquals("Qty:2", get.getElementText(ASSERTPRODUCTQUANTITY));
		   Thread.sleep(3000);
	}
		public void userAbleToClickOnTheCodeOrVoucher() throws InterruptedException {
			Thread.sleep(3000);
			Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", get.getCurrentUrl());
			action.clickOnTheElement(PROMOTIONALCODE);
			}
		//click and collect facility
		public void userClicksFreeClickAndCollect() throws InterruptedException {
			  action.clickOnTheElement(ADDTOBASKETBUTTON);
			   Thread.sleep(3000);
			   action.clickOnTheElement(BASKETFUNCTIONALITYMINI);
			   Thread.sleep(3000);
			   action.clickOnTheElement(VIEWBASKETCHECKOUTBUTTON);
			   Assert.assertEquals("Qty:2", get.getElementText(ASSERTPRODUCTQUANTITY));
			   Thread.sleep(3000);
			 // Assert.assertTrue(get.getElementText(ASSERTTHNAME).toLowerCase().contains("Item description"));
			  action.clickOnTheElement(BASKETFUNCTIONALITYCLICKANDCOLLECT);
			}
		public void userGetDeliveryInformationUpdate() {
			Assert.assertEquals("Delivery information", get.getElementText(ASSERTH1NAME));
		}
}

