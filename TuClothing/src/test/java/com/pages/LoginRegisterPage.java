package com.pages;

import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;

public class LoginRegisterPage extends BaseClass {
	
	
//	login
	private static By EMAILID=By.cssSelector("#j_username");
	private static By PASSWORD=By.cssSelector("#j_password");
	private static By HIDESHOW=By.cssSelector(".hide-show-button");
	private static By FPASSWORD=By.cssSelector("#forgot-password-link");
	private static By FEMAIL=By.cssSelector("#forgot-password");
	private static By SUBMITLOGIN=By.cssSelector("#submit-login");
	private static By ASSERTLOGIN=By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3");
	private static By ASSERTFORGOT=By.cssSelector("#forgot-password-link");
	
//	register
	private static By LOGINREGISTER=By.linkText("Tu Log In / Register");
	private static By REGISTERBUTTON=By.cssSelector(".regToggle");
	private static By REGISTEREMAIL=By.cssSelector("#register_email");
	private static By REGISTERTITLE=By.cssSelector("#register_title");
	private static By REGISTERFNAME=By.cssSelector("#register_firstName");
	private static By REGISTERLNAME=By.cssSelector("#register_lastName");
	private static By REGPASSWORD=By.cssSelector("#password");
	private static By REGCHECKPASSWORD=By.cssSelector("#register_checkPwd");
	private static By REGSUBPASSWORD=By.cssSelector("#submit-register");
	private static By REGNECPOINTS=By.cssSelector("#regNectarPointsOne");
	private static By SUBREGISTER=By.cssSelector("#submit-register");
	private static By ASSERTREGISTERWITHTU=By.cssSelector(".span-12.last h2");
	
	public void validCredentials(DataTable userDetails) throws InterruptedException {
		Map<String, String>loginDetails = userDetails.asMap(String.class, String.class);
		String Email = loginDetails.get("email");
		String Password = loginDetails.get("password");
		Thread.sleep(3000);
		action.updateElement(EMAILID, Email);
         action.updateElement(PASSWORD, Password);
	
	}
	public void helloMessage() {
		 Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
	}
	public void enterInvalidUsernameAndPassword() throws InterruptedException {
	    Thread.sleep(3000);
	    action.updateElement(EMAILID, "abc@gmail.com");
		action.updateElement(PASSWORD, "xyz");
		
       }
	public void warningMessage() {
		Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
		  System.out.println("A warning message should come");
	}
	public void enterInvalidUsernameAndValidPassword() throws InterruptedException {
	
	Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
	Thread.sleep(3000);
	action.updateElement(EMAILID, "test@gmail.com");
	action.updateElement(PASSWORD, "j11223344");
	 System.out.println("Please Fill The Recaptcha");
	}
	public void errorMessage() {
		 Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
	}
	public void enterValidUsernameAndInvalidPassword() throws InterruptedException {
		Thread.sleep(3000);
		action.updateElement(EMAILID, "jikkukalu@gmail.com");
		action.updateElement(PASSWORD, "abcd");
		System.out.println("Please Fill The Recaptcha");
	}
	public void errorMessageDuetoInvalidPassword() {
		Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
	}
	public void enterWithoutUsernameAndPassword() throws InterruptedException {
		     Thread.sleep(3000);
		     action.updateElement(EMAILID, "");
			 action.updateElement(PASSWORD, "");
			 System.out.println("User should see an error message");
	}
	public void errorMessageDuetoMissingData() {
		Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
	}
	public void enterAllValidData() throws InterruptedException {
		Thread.sleep(3000);
		action.updateElement(EMAILID, "jikkukalu@gmail.com");
		action.updateElement(PASSWORD, "j11223344");
		Thread.sleep(3000);
		action.clickOnTheElement(HIDESHOW);
		
	}
	public void asterisksOrBulletsigns() {
		Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
	}
	public void enterValidCredentialsWithoutEnteringRecaptcha() throws InterruptedException {
		Thread.sleep(3000);
		action.updateElement(EMAILID, "jikkukalu@gmail.com");
		action.updateElement(PASSWORD, "j11223344");
			System.out.println("Please check the Recapthcha");
	}
	public void pleaseChecktheRecaptchaMessage() {
		Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
	     }
	public void enterValidUserNameAndWithoutPassword() throws InterruptedException {
		   Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGIN));
			Thread.sleep(3000);
			action.updateElement(EMAILID, "jikkukalu@gmail.com");
			action.updateElement(PASSWORD, "");
		    action.clickOnTheElement(FPASSWORD);
			}
	public void forgottenYourPassword() {
		 Assert.assertEquals("Forgotten your password?", get.getElementText(ASSERTFORGOT));
	}
	//RegisterFunctionality
	private static String baseURL="https://tuclothing.sainsburys.co.uk/";
	public void userIsOnRegistrationPage() {
        Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
	   	action.clickOnTheElement(LOGINREGISTER);
	   	action.clickOnTheElement(REGISTERBUTTON);
		driver.manage().window().maximize();
    }
	  public void userEnterAllValidInformations() {
		  action.updateElement(REGISTEREMAIL, "jikkukalu@gmail.com");
		  action.dropDownByValue(REGISTERTITLE, "mrs");
		  action.updateElement(REGISTERFNAME, "shyno");
		  action.updateElement(REGISTERLNAME, "jithosh");
		 }
    public void enterPasswordAndConfirmPasswordWithSameCharacters() {
    	action.updateElement(REGPASSWORD, "j112");
    	action.updateElement(REGCHECKPASSWORD, "j112");
		action.clickOnTheElement(REGSUBPASSWORD);
	    System.out.println("An Error Message Popped Up");
    }
    public void showAssignedMessage() {
    	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
    }
    public void userEnterNectarpointsWhileRegistering() {
    	action.updateElement(REGISTEREMAIL, "jikkukalu@gmail.com");
    	   action.dropDownByValue(REGISTERTITLE, "mrs");
    	    action.updateElement(REGISTERFNAME, "shyno");
			action.updateElement(REGISTERLNAME, "jithosh");
			action.updateElement(REGPASSWORD, "j1123344");
			action.updateElement(REGCHECKPASSWORD, "j11223344");
			action.updateElement(REGNECPOINTS, "24689");
			action.clickOnTheElement(REGSUBPASSWORD);
			
			System.out.println("An Error Message Popped Up");
    }
    public void shouldAddOnAccount() {
    	 Assert.assertEquals("Register with Tu", get.getElementText(REGISTERTITLE));
    }
    public void userEnterMandatoryFields(DataTable userInformations) throws InterruptedException {
    	 Map<String, String>registerDetails = userInformations.asMap(String.class, String.class);
    	 String Email=registerDetails.get("email");
    	 String FirstName=registerDetails.get("firstname");
    	 String LastName=registerDetails.get("lastname");
    	 String PassWord=registerDetails.get("password");
    	 String ConfirmPassword=registerDetails.get("confirmpassword");
    	    
    	    action.updateElement(REGISTEREMAIL, Email);
			action.dropDownByValue(REGISTERTITLE, "mrs");
			Thread.sleep(3000);
			action.updateElement(REGISTERFNAME, FirstName);
			action.updateElement(REGISTERLNAME, LastName);
			action.updateElement(REGPASSWORD, PassWord);
			Thread.sleep(3000);
			action.updateElement(REGCHECKPASSWORD, ConfirmPassword);
//			action.clickOnTheElement(REGSUBPASSWORD);
			
			 }
    public void shouldAppearRegistrationSuccessfull() {
    	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
    }
    public void userNotEnterOneMandatoryField() {
    	action.updateElement(REGISTEREMAIL, "jikkukalu@gmail.com");
    	action.updateElement(REGISTERFNAME, "shyno");
        action.updateElement(REGISTERLNAME, "jithosh");
	    action.updateElement(REGPASSWORD, "j11223344");
	    action.updateElement(REGCHECKPASSWORD, "j11223344");
	    action.clickOnTheElement(REGSUBPASSWORD);
	    System.out.println("Please select an item in the list");
//	driver.findElement(SUBREGISTER).click();
    }
    public void warningMessagePopup() {
    	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
    }
    public void notClickingRecaptcha() {
    	
		Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
		action.updateElement(REGISTEREMAIL, "jikkukalu@gmail.com");
		action.dropDownByValue(REGISTERTITLE, "mrs");
		action.updateElement(REGISTERFNAME, "shyno");
        action.updateElement(REGISTERLNAME, "jithosh");
	    action.updateElement(REGPASSWORD, "j11223344");
	    action.updateElement(REGCHECKPASSWORD, "j11223344");
	    action.clickOnTheElement(REGSUBPASSWORD);
//		driver.findElement(SUBREGISTER).click();
		System.out.println("Please check the Recaptcha");
		 }
    public void warningMessageAppeared() {
    	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
    	 }
    public void withoutClickingTermsAndConditions() {
    	action.updateElement(REGISTEREMAIL, "jikkukalu@gmail.com");
		action.dropDownByValue(REGISTERTITLE, "mrs");
		action.updateElement(REGISTERFNAME, "shyno");
        action.updateElement(REGISTERLNAME, "jithosh");
	    action.updateElement(REGPASSWORD, "j11223344");
	    action.updateElement(REGCHECKPASSWORD, "j11223344");
	    action.clickOnTheElement(REGSUBPASSWORD);
//	driver.findElement(SUBREGISTER).click();
	System.out.println("An Error Message Popped Up");
    }
    public void promptMessageCame() {
    	 Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
    }
}
