package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class SearchResultsPage extends BaseClass {
	private static By ASSERTH1NAME=By.cssSelector("h1");
	private static By ASSERTMENSWEAR=By.cssSelector(".ln-u-border-bottom--double");
	public void searchResultsForValidProductPage() {
	Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("hair accessories"));
}
	public void searchResultsForInvalidProductPage() {
		Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("bike"));
	}
	public void searchResultsForRightCategories() {
		 Assert.assertEquals("Menswear", get.getElementText(ASSERTMENSWEAR));
	}
}