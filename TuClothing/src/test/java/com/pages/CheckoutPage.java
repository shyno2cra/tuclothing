package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.java.en.When;

public class CheckoutPage extends BaseClass {
//	CheckoutFunctionality
	private static By PROCEEDTOCHECKOUTBUTTON=By.cssSelector("#basketButtonTop");
	private static By GUESTEMAIL=By.cssSelector("#guest_email");
	private static By GUESTCHECKOUT=By.cssSelector("button[data-testid='guest_checkout']");
	private static By CLICKANDCOLLECTBYINDEX=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUEBUTTON=By.cssSelector(".ln-c-button.ln-c-button--primary");
	private static By LOOKUPPOSTCODE=By.cssSelector("#lookup");
	private static By LOOKUPSUBMIT=By.cssSelector("button[data-testid='lookup-submit']");
	private static By SELECTSTORE=By.cssSelector("button[data-testid='select-store']");
	private static By PROCEEDTOSUMMARY=By.cssSelector(".ln-c-button--primary");
	private static By CONTINUETOPAYMENT=By.cssSelector("button[data-testid='continueToPayment']");
	private static By PAYWITHCARD=By.linkText("Pay with a card");
	private static By TITLEDROPDOWN=By.cssSelector("#newTitle");
	private static By FIRSTNAME=By.cssSelector("#newFirstName");
	private static By SURNAME=By.cssSelector("#newSurname");
	private static By CONTACTNUMBER=By.cssSelector("#newContactNumber");
	private static By ADDRESSPOSTCODE=By.cssSelector("#addressPostcode");
	private static By FINDADDRESSBUTTON=By.cssSelector("button[data-testid='findAddress']");
	private static By ADDRESSLISTDROPDOWN=By.cssSelector("#addressListView");
	private static By TICKTOCONTACT=By.cssSelector(".ln-c-form-option__label");
	private static By TERMANDCONDITION=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUETOPAYMENT1=By.cssSelector("#contPayment");
	private static By HOMEDELIVERYBYINDEX=By.cssSelector(".ln-c-form-option__label");
	private static By TITLEDROPDOWN1=By.cssSelector("select[name='titleCode']");
	private static By FIRSTNAME1=By.cssSelector("input[name='firstName']");
	private static By LASTNAME=By.cssSelector("input[name='lastName");
	private static By FINDADDRESSBUTTON1=By.cssSelector(".address-lookup.ln-u-push-bottom");
	private static By ADDRESSLISTDROPDOWN1=By.cssSelector("select[id='addressListView']");
	private static By BILLINGADDRESS=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUEBUTTON1=By.cssSelector("#continue");
	private static By STANDARDDELIVERYBUTTON=By.cssSelector("label[data-testid='deliveryMethod']");
	private static By CONTINUEBUTTON2=By.cssSelector("input[data-testid='continue']");
	//basket functionality refactors
	private static By BASKETFUNCTIONALITYBABY=By.linkText("Baby");
	private static By BASKETFUNCTIONALITYACCESSORIES=By.linkText("Accessories");
	private static By ADDTOBASKETPRODUCTIMAGE=By.cssSelector(".image-component img");
	private static String baseURL="https://tuclothing.sainsburys.co.uk/";
	private static By BASKETFUNCTIONALITYDROPSIZE=By.cssSelector("#select-size");
	private static By BASKETFUNCTIONALITYDROPQUANTITY=By.cssSelector("#productVariantQty");
	private static By ADDTOBASKETBUTTON=By.cssSelector("#AddToCart");
	private static By BASKETFUNCTIONALITYMINI=By.cssSelector("#nav_cart");
	private static By VIEWBASKETCHECKOUTBUTTON=By.cssSelector(".ln-c-button.ln-c-button--primary");
	private static By ASSERTTUDETAILS=By.cssSelector("#tu-product-details");

	public void userIsOnCheckoutPage() throws InterruptedException {
    action.clickOnTheElement(BASKETFUNCTIONALITYBABY);
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby?INITD=GNav-Baby-Header", get.getCurrentUrl());
	action.clickOnTheElement(BASKETFUNCTIONALITYACCESSORIES);
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby-boy-accessories?INITD=GNav-BBW-BoysAccs", get.getCurrentUrl());
	Thread.sleep(3000);
	action.clickOnTheElementByIndex(ADDTOBASKETPRODUCTIMAGE, 0);
	Assert.assertEquals("Product Details", get.getElementText(ASSERTTUDETAILS));
	Thread.sleep(3000);
	action.dropDownByIndex1(BASKETFUNCTIONALITYDROPSIZE, 3);
	//Select sizeDropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPSIZE));
	//sizeDropDown.selectByIndex(3);
	Thread.sleep(3000);
	action.dropDownByValue1(BASKETFUNCTIONALITYDROPQUANTITY, "5");
	//Select quantityDropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPQUANTITY));
	//quantityDropDown.selectByValue("5");
	Thread.sleep(3000);
	action.clickOnTheElement(ADDTOBASKETBUTTON);
	Thread.sleep(3000);
	action.clickOnTheElement(BASKETFUNCTIONALITYMINI);
	action.clickOnTheElement(VIEWBASKETCHECKOUTBUTTON);
	Thread.sleep(3000);
	}
	public void userClicksOnProceedToCheckoutButton() throws InterruptedException {
		action.clickOnTheElement(PROCEEDTOCHECKOUTBUTTON);
		Thread.sleep(3000);
	}
	public void userEnterValidCredentials() throws InterruptedException {
		action.updateElement(GUESTEMAIL, "shyno2cra@gmail.com");
		Thread.sleep(3000);
		action.clickOnTheElement(GUESTCHECKOUT);
		Thread.sleep(3000);
	}
	//home delivery
	
	
}
