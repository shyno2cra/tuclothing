package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

public class ProductDetailPage extends BaseClass {
	private static By ADDTOBASKETKIDSBUTTON=By.linkText("Kids");
	private static By ADDTOBASKETARRIVAL=By.linkText("New Arrivals");
	private static By ADDTOBASKETPRODUCTIMAGE=By.cssSelector(".image-component img");
	private static By ADDTOBASKETDROPSIZE=By.cssSelector("#select-size");
	private static By ADDTOBASKETDROPQUANTITY=By.cssSelector("#productVariantQty");
	private static By ADDTOBASKETBUTTON=By.cssSelector("#AddToCart");
	private static By VIEWBASKETBUTTON=By.cssSelector("#basket-title");
	private static By ADDTOBASKETHAIRCLIPTITLE=By.cssSelector("a[title='Blue Patterned Hair Clips 8 Pack']");
	private static By ADDTOBASKETONESIZE=By.cssSelector("#select-size");
	private static By ADDTOBASKETSUMMERBUTTON=By.linkText("Summer");
	private static By ADDTOBASKETWEATHERPROOFIMAGE=By.cssSelector("img[alt='Womens Weather Proofing']");
	private static By ADDTOBASKETBOOTWELLYIMAGE=By.cssSelector("img[alt='Floral Ankle Boot Wellies ']");
	private static By ADDTOBASKETSELECTCHECKSIZE=By.cssSelector(".selectable");
	private static By ADDTOBASKETLOWLACEIMAGE=By.cssSelector("img[alt='Navy Low Lace Canvas Shoes ']");
	private static By ADDTOBASKETBABYBUTTON=By.linkText("Baby");
	private static By ADDTOBASKETCOATSJACKETBUTTON=By.linkText("Coats & Jackets");
	private static By ADDTOBASKETYELLOWHOODEDIMAGE=By.cssSelector("img[alt='Canary Yellow Hooded Mac (0-24 Months)']");
	private static String baseURL="https://tuclothing.sainsburys.co.uk/";
	private static By ASSERTKIDSBASKET=By.cssSelector(".ln-u-border-bottom--double");
	private static By ASSERTPRODUCTHEADLINE=By.cssSelector(".productHeadline");
	private static By ASSERTPRODUCTQUANTITY=By.cssSelector(".prod_quantity");
	private static By SEARCHBUTTON=By.cssSelector(".searchButton");
	private static By SEARCHBOX=By.cssSelector("#search");
	
	public void userIsOnProductDetailPage() throws InterruptedException {
    action.clickOnTheElement(ADDTOBASKETKIDSBUTTON);
	Assert.assertEquals("Kids", get.getElementText(ASSERTKIDSBASKET));
	driver.manage().window().maximize();
	//add product to basket with login
	action.updateElement(SEARCHBOX, "Blue Patterned Hair Clips 8 Pack");
	action.clickOnTheElement(SEARCHBUTTON);
	//multiple items
	action.clickOnTheElement(ADDTOBASKETSUMMERBUTTON);
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/holiday-shop?INITD=GNav-HolShop-Header", get.getCurrentUrl());
	//adding valid product and basket button functionality
	action.clickOnTheElement(ADDTOBASKETBABYBUTTON);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby?INITD=GNav-Baby-Header", get.getCurrentUrl());

	}
	
	public void userTheSelectSizeAndTheQuantity() throws InterruptedException {
		action.clickOnTheElement(ADDTOBASKETARRIVAL);
	
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby-new-in?q=%3Anewarrivals%3Astyle%3ABaby+Boys%3Astyle%3ABaby+Unisex&INITD=GNav-BBW-Boys", get.getCurrentUrl());
	Thread.sleep(3000);
	action.clickOnTheElementByIndex(ADDTOBASKETPRODUCTIMAGE, 2);
	
	Assert.assertEquals("Charcoal Grey Gauze Cheesecloth Trouser (Newborn-24 Months)", get.getElementText(ASSERTPRODUCTHEADLINE));
	Thread.sleep(3000);
	action.dropDownByIndex1(ADDTOBASKETDROPSIZE, 6);
//	Select sizeDropdown=new Select(driver.findElement(ADDTOBASKETDROPSIZE));
//	sizeDropdown.selectByIndex(3);
	Thread.sleep(3000);
	action.dropDownByValue1(ADDTOBASKETDROPQUANTITY, "1");
//	Select quantityDropdown=new Select(driver.findElement(ADDTOBASKETDROPQUANTITY));
//	quantityDropdown.selectByValue("1");
	Thread.sleep(3000);
	
		}
	
	public void clickOnAddBasket() throws InterruptedException {
		action.clickOnTheElement(ADDTOBASKETBUTTON);
		Thread.sleep(3000);
		action.clickOnTheElement(VIEWBASKETBUTTON);
		Thread.sleep(3000);
	}
	public void productShouldAddedInTheBasket() throws InterruptedException {
		Thread.sleep(3000);
		Assert.assertEquals("Qty:1", get.getElementText(ASSERTPRODUCTQUANTITY));
	}
	//add product with login
	public void userSelectTheSizeAndQuantity() throws InterruptedException {
//		action.updateElement(SEARCHBOX, "Blue Patterned Hair Clips 8 Pack");
//		action.clickOnTheElement(SEARCHBUTTON);
		action.clickOnTheElement(ADDTOBASKETHAIRCLIPTITLE);
		Assert.assertEquals("Blue Patterned Hair Clips 8 Pack", get.getElementText(ASSERTPRODUCTHEADLINE));
		action.clickOnTheElement(ADDTOBASKETONESIZE);
		Thread.sleep(3000);
		action.dropDownByValue1(ADDTOBASKETDROPQUANTITY, "2");
//		Select quantityDropDown=new Select(driver.findElement(ADDTOBASKETDROPQUANTITY));
//		quantityDropDown.selectByValue("1");
		Thread.sleep(3000);
		action.clickOnTheElement(ADDTOBASKETBUTTON);
		Thread.sleep(3000);
		action.clickOnTheElement(VIEWBASKETBUTTON);
		Thread.sleep(3000);
	}
	public void productShouldAddInTheBasket() throws InterruptedException {
		Thread.sleep(3000);
		Assert.assertEquals("Qty:4", get.getElementText(ASSERTPRODUCTQUANTITY));
	}
	//multiple items to basket
	public void userSelectOneProductSizeAndQuantity() throws InterruptedException {
		Thread.sleep(3000);
		action.clickOnTheElement(ADDTOBASKETWEATHERPROOFIMAGE);
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/women/women-weather-proofing?INITD=holshop_ww_weatherproofing", get.getCurrentUrl());
		action.clickOnTheElement(ADDTOBASKETBOOTWELLYIMAGE);
	     Thread.sleep(3000);
	    Assert.assertEquals("Floral Ankle Boot Wellies", get.getElementText(ASSERTPRODUCTHEADLINE));
	    Thread.sleep(3000);
	    action.selectCheckSizeIndex(ADDTOBASKETSELECTCHECKSIZE, 1);
	    Thread.sleep(3000);
	    action.dropDownByValue1(ADDTOBASKETDROPQUANTITY, "1");
//	    Select quantityDropDown=new Select(driver.findElement(ADDTOBASKETDROPQUANTITY));
//	    quantityDropDown.selectByValue("1");
	    action.clickOnTheElement(ADDTOBASKETBUTTON);
	    Thread.sleep(3000);
	}
	public void clickOnOtherItems() throws InterruptedException {
		action.updateElement(SEARCHBOX, "Blue Patterned Hair Clips 8 Pack");
		action.clickOnTheElement(SEARCHBUTTON);
		action.clickOnTheElement(ADDTOBASKETHAIRCLIPTITLE);
		Assert.assertEquals("Blue Patterned Hair Clips 8 Pack", get.getElementText(ASSERTPRODUCTHEADLINE));
		action.clickOnTheElement(ADDTOBASKETONESIZE);
		Thread.sleep(3000);
		action.dropDownByValue1(ADDTOBASKETDROPQUANTITY, "2");
//		Select quantityDropDown=new Select(driver.findElement(ADDTOBASKETDROPQUANTITY));
//		quantityDropDown.selectByValue("1");
		Thread.sleep(3000);
		
		}
	public void hitAddBasket() {
		action.clickOnTheElement(ADDTOBASKETBUTTON);
	}
	public void addBasketShouldHighlight() {
		action.clickOnTheElement(VIEWBASKETBUTTON);
	}
	public void productShouldAddInBasket() {
		Assert.assertEquals("Qty:1", get.getElementText(ASSERTPRODUCTQUANTITY));
	}
	public void userSelectNeededProductsSizeAndQuantity() throws InterruptedException {
		action.clickOnTheElement(ADDTOBASKETCOATSJACKETBUTTON);
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby-boy-coats-and-jackets?INITD=GNav-BBW-BoysCoats", get.getCurrentUrl());
		Thread.sleep(3000);
		action.clickOnTheElement(ADDTOBASKETYELLOWHOODEDIMAGE);
		Assert.assertEquals("Canary Yellow Hooded Mac (0-24 Months)", get.getElementText(ASSERTPRODUCTHEADLINE));
		Thread.sleep(3000);
		action.dropDownByIndex1(ADDTOBASKETDROPSIZE, 3);
//		Select sizeDropDown=new Select(driver.findElement(ADDTOBASKETDROPSIZE));
//		sizeDropDown.selectByIndex(3);
		Thread.sleep(3000);
		action.dropDownByValue1(ADDTOBASKETDROPQUANTITY, "2");
//	Select quantityDropDown=new Select(driver.findElement(ADDTOBASKETDROPQUANTITY));
//		   quantityDropDown.selectByValue("2");
		
		action.clickOnTheElement(ADDTOBASKETBUTTON);
		Thread.sleep(3000);
		action.clickOnTheElement(VIEWBASKETBUTTON);
	}
	public void addBasketFunctionShouldEnabled() throws InterruptedException {
		Thread.sleep(3000);
		Assert.assertEquals("Qty:2", get.getElementText(ASSERTPRODUCTQUANTITY));
	}
}
