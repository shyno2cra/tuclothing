package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

public class StoreLocatorPage extends BaseClass {
//	store locator
	private static By STORELOCATOR=By.linkText("Tu Store Locator");
	private static By POSTCODE=By.cssSelector(".ln-c-text-input.ln-u-push-bottom");
	private static By STOREWOMEN=By.cssSelector("label[for='women']");
	private static By STOREMEN=By.cssSelector("label[for='men']");
	private static By STORECHILDREN=By.cssSelector("label[for='children']");
	private static By STORECLICK=By.cssSelector("label[for='click']");
	private static By STOREFINDER=By.cssSelector("#tuStoreFinderForm .ln-c-button.ln-c-button--primary");
	private static By ASSERTH1NAME=By.cssSelector("h1");
	private static By ASSERTH3STORENAME=By.cssSelector("#store_locator #header1 h3");
	private static By ASSERTINVALIDPC=By.cssSelector(".ln-u-h4.ln-u-flush-bottom");
	
	private static String baseURL="https://tuclothing.sainsburys.co.uk/";
	
	public void userIsOnStoreLocatorPage() {
		System.setProperty("webdriver.chrome.driver", "D:\\Workspace\\chromedriver_win32\\chromedriver.exe");
	    driver=new ChromeDriver();
	    driver.get(baseURL);
	   	driver.manage().window().maximize();
	   	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
	   	action.clickOnTheElement(STORELOCATOR);
	   
	}
	public void functionalityEnterValidPostCode() throws InterruptedException {
		Thread.sleep(3000);
		action.updateElement(POSTCODE, "MK430PG");
		Thread.sleep(3000);
		action.clickOnTheElement(STOREWOMEN);
		action.clickOnTheElement(STOREMEN);
		action.clickOnTheElement(STORECHILDREN);
		action.clickOnTheElement(STORECLICK);
		action.clickOnTheElement(STOREFINDER);
		}
	public void shouldDisplayNearestStores() {
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
	}
	public void userEnterPostCode() throws InterruptedException {
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
		action.updateElement(POSTCODE, "MK430PG");
		Thread.sleep(3000);
		action.clickOnTheElement(STOREFINDER);
		
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTH3STORENAME));
	}
	public void openinghoursAndNearestStores() {
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTH3STORENAME));
	}
	public void userEnterAValidPostCode() throws InterruptedException {
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
		action.updateElement(POSTCODE, "MK430PG");
		Thread.sleep(3000);
		action.clickOnTheElement(STOREWOMEN);
		action.clickOnTheElement(STOREMEN);
		action.clickOnTheElement(STORECHILDREN);
		action.clickOnTheElement(STORECLICK);
		action.clickOnTheElement(STOREFINDER);
		 }
	public void userAbleToSeeStockClothingClickAndCollect() {
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTH3STORENAME));
	}
	public void userEnterAnInvalidPostCode() throws InterruptedException {
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
		action.updateElement(POSTCODE, "MKPG");
		Thread.sleep(3000);
		action.clickOnTheElement(STOREWOMEN);
		action.clickOnTheElement(STOREMEN);
		action.clickOnTheElement(STORECHILDREN);
		action.clickOnTheElement(STORECLICK);
		action.clickOnTheElement(STOREFINDER);
		System.out.println("A warning message should come");
		}
	public void shouldShowAWarningMessage() {
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
		Assert.assertEquals("Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.", get.getElementText(ASSERTINVALIDPC));
	}
	public void userNotEnteringPostCode() throws InterruptedException {
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
		action.updateElement(POSTCODE, " ");
		Thread.sleep(3000);
		action.clickOnTheElement(STOREWOMEN);
		action.clickOnTheElement(STOREMEN);
		action.clickOnTheElement(STORECHILDREN);
		action.clickOnTheElement(STORECLICK);
		System.out.println("A warning message should come");
	}
	public void clickFindStores() {
		action.clickOnTheElement(STOREFINDER);
	}
		public void popupOfWarningMessage() {
			Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
		}
		public void notClickingTheGivenOptions() {
			Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
			action.updateElement(POSTCODE, "MK430PG");
			driver.findElement(STOREFINDER).click();
			System.out.println("Shown Nearest Stores");
		}
		public void shouldShowNearByStores() {
			Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
		}
}
