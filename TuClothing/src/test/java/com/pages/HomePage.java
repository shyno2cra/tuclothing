package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

public class HomePage extends BaseClass {
//	public static WebDriver driver;
	private static String baseURL="https://tuclothing.sainsburys.co.uk/";
	//searchfunctionality
	private static By SEARCHBUTTON=By.cssSelector(".searchButton");
	private static By SEARCHBOX=By.cssSelector("#search");
	private static By WARNINGMESSAGEFORBLANKDATA=By.cssSelector("span[alt='Please complete a product search']");
	//loginfunctionality
	private static By LOGINREGISTER=By.linkText("Tu Log In / Register");
    public void verifyHomePage() {
    	System.setProperty("webdriver.chrome.driver", "D:\\Workspace\\chromedriver_win32\\chromedriver.exe");
   	 driver=new ChromeDriver();
   	driver.get(baseURL);
   	driver.manage().window().maximize();
   	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
    }
    public void searchForAValidProduct(String searchWord) {
    	action.updateElement(SEARCHBOX, searchWord);
    	action.clickOnTheElement(SEARCHBUTTON);
    	
         }
    public void searchForInvalidProduct() {
    	action.updateElement(SEARCHBOX, "Bike");
    	action.clickOnTheElement(SEARCHBUTTON);
		
    }
    public void searchForBlankProduct() {
    	action.updateElement(SEARCHBOX, "");
    	action.clickOnTheElement(SEARCHBUTTON);
		 }
    public void warningMessage() {
    	 Assert.assertEquals("Please complete a product search", get.getElementText(WARNINGMESSAGEFORBLANKDATA));
    	 System.out.println("please complete a product search ");
    }
    public void searchWithRightCategories() {
    	action.updateElement(SEARCHBOX, "Menswear");
    	action.clickOnTheElement(SEARCHBUTTON);
		
    }
    public void searchBarFunctionality() {
    	action.updateElement(SEARCHBOX, "");
    	action.clickOnTheElement(SEARCHBOX);
		
    }
    public void searchBarHighlight() {
    	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
    }
    public void loginRegister() {
    	action.clickOnTheElement(LOGINREGISTER);
    	
    	 }
    
   
}
