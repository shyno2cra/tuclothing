package com.runner;

import org.openqa.selenium.WebDriver;

import com.driver.Action;
import com.driver.Get;
import com.pages.BasketPage;
import com.pages.CheckoutPage;
import com.pages.DeliveryPage;
import com.pages.HomePage;
import com.pages.LoginRegisterPage;
import com.pages.PaymentPage;
import com.pages.ProductDetailPage;
import com.pages.ProductListingPage;
import com.pages.SearchResultsPage;
import com.pages.StoreLocatorPage;

public class BaseClass {
	public static WebDriver driver;
	public static HomePage homepage = new HomePage();
	public static SearchResultsPage searchresultspage = new SearchResultsPage();
	public static LoginRegisterPage loginregisterpage = new LoginRegisterPage(); 
	public static StoreLocatorPage storelocatorpage = new StoreLocatorPage();
	public static Get get = new Get();
	public static Action action = new Action();
	public static ProductListingPage productlistingpage = new ProductListingPage();
	public static ProductDetailPage productdetailpage = new ProductDetailPage();
	public static BasketPage basketpage = new BasketPage();
	public static CheckoutPage checkoutpage = new CheckoutPage();
	public static DeliveryPage deliverypage = new DeliveryPage();
	public static PaymentPage paymentpage = new PaymentPage();
}
