package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProductDetailPageStepDef extends BaseClass {
	
	@Given("^User is on product detail page$")
	public void user_is_on_product_detail_page() throws Throwable {
		homepage.verifyHomePage();
		productdetailpage.userIsOnProductDetailPage();
	   }
	@When("^User the select size and the quantity$")
	public void user_the_select_size_and_the_quantity() throws Throwable {
     productdetailpage.userTheSelectSizeAndTheQuantity();
		 }

@When("^click on add basket$")
public void click_on_add_basket() throws Throwable {
	productdetailpage.clickOnAddBasket();
	}
@Then("^product should added in the basket$")
public void product_should_added_in_the_basket() throws Throwable {
	productdetailpage.productShouldAddedInTheBasket();
}
@When("^User select the size and quantity$")
public void user_select_the_size_and_quantity() throws Throwable {
	productdetailpage.userSelectTheSizeAndQuantity();
	}
@Then("^product should add in the basket$")
public void product_should_add_in_the_basket() throws Throwable {
	productdetailpage.productShouldAddInTheBasket();
    }
@When("^User select one product, size and quantity$")
public void user_select_one_product_size_and_quantity() throws Throwable {
	productdetailpage.userSelectOneProductSizeAndQuantity();
	}
@When("^click on other items$")
public void click_on_other_items() throws Throwable {
	 productdetailpage.clickOnOtherItems();
		
  }
@When("^hit add basket$")
public void hit_add_basket() throws Throwable {
	productdetailpage.hitAddBasket();
    }
@Then("^add basket should highlight$")
public void add_basket_should_highlight() throws Throwable {
	productdetailpage.addBasketShouldHighlight();
    }
@Then("^products should add in basket$")
public void products_should_add_in_basket() throws Throwable {
	productdetailpage.productShouldAddInBasket();
    }
@When("^User select needed products, size and quantity$")
public void user_select_needed_products_size_and_quantity() throws Throwable {
	productdetailpage.userSelectNeededProductsSizeAndQuantity();
	}
@Then("^add basket function should enabled$")
public void add_basket_function_should_enabled() throws Throwable {
	productdetailpage.addBasketFunctionShouldEnabled();
    }




}
