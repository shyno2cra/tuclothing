package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStepDef extends BaseClass {
	@Given("^User in home page$")
	public void user_in_home_page() throws Throwable {
		homepage.verifyHomePage();
	}
//	@When("^User search for a valid product$")
//	public void user_search_for_a_valid_product() throws Throwable {
//		homepage.searchForAValidProduct();
//	   }
	@When("^User search for \"([^\"]*)\" keyword$")
	public void user_search_for_keyword(String searchWord) throws Throwable {
	    	    homepage.searchForAValidProduct(searchWord);
	}
	@When("^User search for invalid product$")
	public void user_search_for_invalid_product() throws Throwable {
		homepage.searchForInvalidProduct();
	   }
	@When("^User search for a blank product$")
	public void user_search_for_a_blank_product() throws Throwable {
		homepage.searchForBlankProduct();
	    }
	 @Then("^should appear a warning message$")
		public void should_appear_a_warning_message() throws Throwable {
		 homepage.warningMessage();
		    }
	 @When("^User search with categories$")
		public void user_search_with_categories() throws Throwable {
			homepage.searchWithRightCategories();
		   }
	 @When("^User click on search bar$")
	  public void user_click_on_search_bar() throws Throwable {
		  homepage.searchBarFunctionality();
			}
	 @Then("^search box should highlight$")
	  public void search_box_should_highlight() throws Throwable {
		   homepage.searchBarHighlight();
	       }
	 @When("^User click on login/register$")
	  public void user_click_on_login_register() throws Throwable {
		  homepage.loginRegister();
			 }
	
	 
}
