package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllTestCases {
	public static WebDriver driver;
	private static String baseURL="https://tuclothing.sainsburys.co.uk/";
//	search
	private static By SEARCHBUTTON=By.cssSelector(".searchButton");
	private static By SEARCHBOX=By.cssSelector("#search");
//	login
	private static By LOGINREGISTER=By.linkText("Tu Log In / Register");
	private static By EMAILID=By.cssSelector("#j_username");
	private static By PASSWORD=By.cssSelector("#j_password");
	private static By HIDESHOW=By.cssSelector(".hide-show-button");
	private static By FPASSWORD=By.cssSelector("#forgot-password-link");
	private static By FEMAIL=By.cssSelector("#forgot-password");
	private static By SUBMITLOGIN=By.cssSelector("#submit-login");
//	register
	private static By REGISTERBUTTON=By.cssSelector(".regToggle");
	private static By REGISTEREMAIL=By.cssSelector("#register_email");
	private static By REGISTERTITLE=By.cssSelector("#register_title");
	private static By REGISTERFNAME=By.cssSelector("#register_firstName");
	private static By REGISTERLNAME=By.cssSelector("#register_lastName");
	private static By REGPASSWORD=By.cssSelector("#password");
	private static By REGCHECKPASSWORD=By.cssSelector("#register_checkPwd");
	private static By REGSUBPASSWORD=By.cssSelector("#submit-register");
	private static By REGNECPOINTS=By.cssSelector("#regNectarPointsOne");
	private static By SUBREGISTER=By.cssSelector("#submit-register");
//	store locator
	private static By STORELOCATOR=By.linkText("Tu Store Locator");
	private static By POSTCODE=By.cssSelector(".ln-c-text-input.ln-u-push-bottom");
	private static By STOREWOMEN=By.cssSelector("label[for='women']");
	private static By STOREMEN=By.cssSelector("label[for='men']");
	private static By STORECHILDREN=By.cssSelector("label[for='children']");
	private static By STORECLICK=By.cssSelector("label[for='click']");
	private static By STOREFINDER=By.cssSelector("#tuStoreFinderForm .ln-c-button.ln-c-button--primary");
//	add to basket
	private static By ADDTOBASKETKIDSBUTTON=By.linkText("Kids");
	private static By ADDTOBASKETARRIVAL=By.linkText("New Arrivals");
	private static By ADDTOBASKETPRODUCTIMAGE=By.cssSelector(".image-component img");
	private static By ADDTOBASKETDROPSIZE=By.cssSelector("#select-size");
	private static By ADDTOBASKETDROPQUANTITY=By.cssSelector("#productVariantQty");
	private static By ADDTOBASKETBUTTON=By.cssSelector("#AddToCart");
	private static By VIEWBASKETBUTTON=By.cssSelector("#basket-title");
	private static By ADDTOBASKETHAIRCLIPTITLE=By.cssSelector("a[title='Multicoloured Glitter Wavy Hair Clips 10 Pack']");
	private static By ADDTOBASKETONESIZE=By.cssSelector("#select-size");
	private static By ADDTOBASKETSUMMERBUTTON=By.linkText("Summer");
	private static By ADDTOBASKETWEATHERPROOFIMAGE=By.cssSelector("img[alt='Womens Weather Proofing']");
	private static By ADDTOBASKETBOOTWELLYIMAGE=By.cssSelector("img[alt='Floral Ankle Boot Wellies ']");
	private static By ADDTOBASKETSELECTCHECKSIZE=By.cssSelector(".selectable");
	private static By ADDTOBASKETLOWLACEIMAGE=By.cssSelector("img[alt='Navy Low Lace Canvas Shoes ']");
	private static By ADDTOBASKETBABYBUTTON=By.linkText("Baby");
	private static By ADDTOBASKETCOATSJACKETBUTTON=By.linkText("Coats & Jackets");
	private static By ADDTOBASKETYELLOWHOODEDIMAGE=By.cssSelector("img[alt='Canary Yellow Hooded Mac (0-24 Months)']");
//	BasketFunctionality
	private static By BASKETFUNCTIONALITYBRAND=By.linkText("Brands");
	private static By BASKETFUNCTIONALITYJEWLRYWATCHES=By.linkText("JEWELLERY & WATCHES");
	private static By BASKETFUNCTIONALITYONESIZE=By.cssSelector("#select-size");
	private static By BASKETFUNCTIONALITYDROPQUANTITY=By.cssSelector("#productVariantQty");
	private static By BASKETFUNCTIONALITYMINI=By.cssSelector("#nav_cart");
	private static By BASKETFUNCTIONALITYUNIFORM=By.linkText("School Uniform");
	private static By BASKETFUNCTIONALITYCARDIGAN=By.linkText("Cardigans");
	private static By BASKETFUNCTIONALITYCOLOUR=By.cssSelector(".tu-pdp__information-selector-link");
	private static By BASKETFUNCTIONALITYDROPSIZE=By.cssSelector("#select-size");
	private static By BASKETFUNCTIONALITYSOCKS=By.cssSelector("img['Grey Knee High Sock 5 Pack (6 Infant-5.5 Adult)']");
	private static By BASKETFUNCTIONALITYLOUNGE=By.linkText("Loungewear");
   private static By BASKETFUNCTIONALITYSLIPPERIMAGE=By.cssSelector("img[alt='Pink Stripy Open-Toe Cupsole Slippers']");
	private static By BASKETFUNCTIONALITYCHECKSIZE=By.cssSelector(".selectable");
	private static By VIEWBASKETCHECKOUTBUTTON=By.cssSelector(".ln-c-button.ln-c-button--primary");
	private static By CONTINUESHOPPING=By.cssSelector(".ln-c-button.ln-c-button--secondary.left");
	private static By PROMOTIONALCODE=By.cssSelector(".basketTotalsAddPromo.clearfix #showPromo");
	private static By BASKETFUNCTIONALITYBABY=By.linkText("Baby");
	private static By BASKETFUNCTIONALITYACCESSORIES=By.linkText("Accessories");
	private static By BASKETFUNCTIONALITYCLICKANDCOLLECT=By.linkText("Click & Collect");
	private static By BASKETFUNCTIONALITYRETURNTOSTORES=By.cssSelector("a[href='/help/returnsAndRefunds']");
	private static By MINIBASKETICON=By.cssSelector(".icon");
	private static By REMOVEITEM=By.cssSelector("#RemoveProduct_0");
//	CheckoutFunctionality
	private static By PROCEEDTOCHECKOUTBUTTON=By.cssSelector("#basketButtonTop");
	private static By GUESTEMAIL=By.cssSelector("#guest_email");
	private static By GUESTCHECKOUT=By.cssSelector("button[data-testid='guest_checkout']");
	private static By CLICKANDCOLLECTBYINDEX=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUEBUTTON=By.cssSelector(".ln-c-button.ln-c-button--primary");
	private static By LOOKUPPOSTCODE=By.cssSelector("#lookup");
	private static By LOOKUPSUBMIT=By.cssSelector("button[data-testid='lookup-submit']");
	private static By SELECTSTORE=By.cssSelector("button[data-testid='select-store']");
	private static By PROCEEDTOSUMMARY=By.cssSelector(".ln-c-button--primary");
	private static By CONTINUETOPAYMENT=By.cssSelector("button[data-testid='continueToPayment']");
	private static By PAYWITHCARD=By.linkText("Pay with a card");
	private static By TITLEDROPDOWN=By.cssSelector("#newTitle");
	private static By FIRSTNAME=By.cssSelector("#newFirstName");
	private static By SURNAME=By.cssSelector("#newSurname");
	private static By CONTACTNUMBER=By.cssSelector("#newContactNumber");
	private static By ADDRESSPOSTCODE=By.cssSelector("#addressPostcode");
	private static By FINDADDRESSBUTTON=By.cssSelector("button[data-testid='findAddress']");
	private static By ADDRESSLISTDROPDOWN=By.cssSelector("#addressListView");
	private static By TICKTOCONTACT=By.cssSelector(".ln-c-form-option__label");
	private static By TERMANDCONDITION=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUETOPAYMENT1=By.cssSelector("#contPayment");
	private static By HOMEDELIVERYBYINDEX=By.cssSelector(".ln-c-form-option__label");
	private static By TITLEDROPDOWN1=By.cssSelector("select[name='titleCode']");
	private static By FIRSTNAME1=By.cssSelector("input[name='firstName']");
	private static By LASTNAME=By.cssSelector("input[name='lastName");
	private static By FINDADDRESSBUTTON1=By.cssSelector(".address-lookup.ln-u-push-bottom");
	private static By ADDRESSLISTDROPDOWN1=By.cssSelector("select[id='addressListView']");
	private static By BILLINGADDRESS=By.cssSelector(".ln-c-form-option__label");
	private static By CONTINUEBUTTON1=By.cssSelector("#continue");
	private static By STANDARDDELIVERYBUTTON=By.cssSelector("label[data-testid='deliveryMethod']");
	private static By CONTINUEBUTTON2=By.cssSelector("input[data-testid='continue']");
	
	
		
		
	  

	

	
	

	

	
 
 
  
//  @When("^User click on particular search bar$")
//  public void user_click_on_particular_search_bar() throws Throwable {
//	  driver.findElement(SEARCHBUTTON).click();
//      }
//
//  @Then("^search bar should highlight in black colour$")
//  public void search_bar_should_highlight_in_black_colour() throws Throwable {
//	  Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//      }
 

  

 
 

 
  

  
  

 

 



// @Then("^?show password? facility should appear$")
// public void show_password_facility_should_appear() throws Throwable {
//	 Assert.assertEquals("ShowShow", driver.findElement(By.cssSelector(".hide-show-button")).getText());
//     
// }


 


 
//	 System.setProperty("webdriver.chrome.driver", "D:\\Workspace\\chromedriver_win32\\chromedriver.exe");
//	 driver=new ChromeDriver();
//	driver.get(baseURL);
//	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//	driver.manage().window().maximize();
     
 

 
 


     
 

 


 

// @Then("^it should optional to add$")
// public void it_should_optional_to_add() throws Throwable {
//     }
 

 

 
 

 
//		driver.findElement(SUBREGISTER).click();
		
    


 
//	 Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
	// Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-register")).getText());
  //   }









//@Then("^options for stores that stock clothing$")
//public void options_for_stores_that_stock_clothing() throws Throwable {
//    }













//add product with login


//multiple items to add basket






//adding valid product to basket



//viewing basket and adding product to basket:basket functionality

	
	







	//Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/loungewear/loungewear?INITD=dd_loungewear", driver.getCurrentUrl());
	
//promotional code:basket functionality



//free click and collect















//end to end functionality
//@Given("^User is on home page$")
//public void user_is_on_home_page() throws Throwable {
//	System.setProperty("webdriver.chrome.driver", "D:\\Workspace\\chromedriver_win32\\chromedriver.exe");
//	 driver=new ChromeDriver();
//	driver.get(baseURL);
//	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//	driver.manage().window().maximize();
//}

@When("^User selects any product name$")
public void user_selects_any_product_name() throws Throwable {
	driver.findElement(BASKETFUNCTIONALITYBABY).click();
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby?INITD=GNav-Baby-Header", driver.getCurrentUrl());
	driver.findElement(BASKETFUNCTIONALITYACCESSORIES).click();
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby-boy-accessories?INITD=GNav-BBW-BoysAccs", driver.getCurrentUrl());
	Thread.sleep(3000);
	
}
@When("^enter into the product detail page$")
public void enter_into_the_product_detail_page() throws Throwable {
	driver.findElements(ADDTOBASKETPRODUCTIMAGE).get(3).click();
	Assert.assertEquals("Product Details", driver.findElement(By.cssSelector("#tu-product-details")).getText());
	Thread.sleep(3000); 
}

@When("^select size and quantity$")
public void select_size_and_quantity() throws Throwable {
	Select sizeDropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPSIZE));
	sizeDropDown.selectByIndex(3);
	Thread.sleep(3000);
	Select quantityDropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPQUANTITY));
	quantityDropDown.selectByValue("6");
	Thread.sleep(3000);
}

@When("^click on add basket box$")
public void click_on_add_basket_box() throws Throwable {
	driver.findElement(ADDTOBASKETBUTTON).click();
	Thread.sleep(3000);
	
}

@When("^click the mini basket icon which shows view basket and check out$")
public void click_the_mini_basket_icon_which_shows_view_basket_and_check_out() throws Throwable {
	driver.findElement(BASKETFUNCTIONALITYMINI).click();
	driver.findElement(VIEWBASKETCHECKOUTBUTTON).click();
	Thread.sleep(3000);
}

@When("^hit on proceed to checkout button$")
public void hit_on_proceed_to_checkout_button() throws Throwable {
	driver.findElement(PROCEEDTOCHECKOUTBUTTON).click();
	Thread.sleep(3000);
}

@When("^enter the valid email as a guest on new Tu customer page$")
public void enter_the_valid_email_as_a_guest_on_new_Tu_customer_page() throws Throwable {
	driver.findElement(GUESTEMAIL).clear();
	driver.findElement(GUESTEMAIL).sendKeys("shyno2cra@gmail.com");
	Thread.sleep(3000);
	
}

@When("^click on continue button$")
public void click_on_continue_button() throws Throwable {
	driver.findElement(GUESTCHECKOUT).click();
	Thread.sleep(3000);
}

@When("^direct to click and collect button$")
public void direct_to_click_and_collect_button() throws Throwable {
	driver.findElements(CLICKANDCOLLECTBYINDEX).get(0).click();
	Assert.assertEquals("Delivery options", driver.findElement(By.cssSelector(".checkout-step-title")).getText());
	driver.findElement(CONTINUEBUTTON).click();
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/pickup-location/findStore", driver.getCurrentUrl());
	driver.findElement(LOOKUPPOSTCODE).clear();
	driver.findElement(LOOKUPPOSTCODE).sendKeys("MK430PG");
	driver.findElement(LOOKUPSUBMIT).click();
	Thread.sleep(3000);
	driver.findElements(SELECTSTORE).get(0).click();
	driver.findElement(PROCEEDTOSUMMARY).click();
	Thread.sleep(3000);
	driver.findElement(CONTINUETOPAYMENT).click();
	Thread.sleep(3000);
}

@Then("^should navigate to the payment details$")
public void should_navigate_to_the_payment_details() throws Throwable {
	driver.findElement(PAYWITHCARD).click();
	Select titleDropDown=new Select(driver.findElement(TITLEDROPDOWN));
	titleDropDown.selectByValue("mrs");
	driver.findElement(FIRSTNAME).clear();
	driver.findElement(FIRSTNAME).sendKeys("Shyno");
	driver.findElement(SURNAME).clear();
	driver.findElement(SURNAME).sendKeys("Jithosh");
	driver.findElement(CONTACTNUMBER).sendKeys("07417513636");
	driver.findElement(ADDRESSPOSTCODE).sendKeys("Mk430PG");
	driver.findElement(FINDADDRESSBUTTON).click();
	Thread.sleep(3000);
	Select addressDropDown=new Select(driver.findElement(ADDRESSLISTDROPDOWN));
	addressDropDown.selectByValue("5");
	Thread.sleep(3000);
	Assert.assertEquals("Staying in touch", driver.findElement(By.cssSelector(".checkout-step-subtitle")).getText());
	driver.findElements(TICKTOCONTACT).get(0).click();
	driver.findElements(TERMANDCONDITION).get(1).click();
	driver.findElement(CONTINUETOPAYMENT1).click();
	System.out.println("Terms And Conditions Policy Page Popped Up");
}
//endtoend home delivery
@When("^User selects product name$")
public void user_selects_product_name() throws Throwable {
	driver.findElement(BASKETFUNCTIONALITYBABY).click();
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby?INITD=GNav-Baby-Header", driver.getCurrentUrl());
	driver.findElement(BASKETFUNCTIONALITYACCESSORIES).click();
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/baby/baby-boy-accessories?INITD=GNav-BBW-BoysAccs", driver.getCurrentUrl());
	Thread.sleep(3000);
}

@When("^enter into product detail page$")
public void enter_into_product_detail_page() throws Throwable {
	driver.findElements(ADDTOBASKETPRODUCTIMAGE).get(3).click();
	Assert.assertEquals("Product Details", driver.findElement(By.cssSelector("#tu-product-details")).getText());
	Thread.sleep(3000);
}

@When("^select the size and quantity$")
public void select_the_size_and_quantity() throws Throwable {
	Select sizeDropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPSIZE));
	sizeDropDown.selectByIndex(3);
	Thread.sleep(3000);
	Select quantityDropDown=new Select(driver.findElement(BASKETFUNCTIONALITYDROPQUANTITY));
	quantityDropDown.selectByValue("2");
	Thread.sleep(3000);
}

@When("^click on add to basket box$")
public void click_on_add_to_basket_box() throws Throwable {
	driver.findElement(ADDTOBASKETBUTTON).click();
	Thread.sleep(3000);
}

@When("^click the mini basket icon which shows view basket and the check out$")
public void click_the_mini_basket_icon_which_shows_view_basket_and_the_check_out() throws Throwable {
	driver.findElement(BASKETFUNCTIONALITYMINI).click();
	driver.findElement(VIEWBASKETCHECKOUTBUTTON).click();
	Thread.sleep(3000);
}

@When("^once select the options and click continue button$")
public void once_select_the_options_and_click_continue_button() throws Throwable {
	driver.findElement(PROCEEDTOCHECKOUTBUTTON).click();
	Thread.sleep(3000);
}

@When("^enter the valid email as guest on new Tu customer page$")
public void enter_the_valid_email_as_guest_on_new_Tu_customer_page() throws Throwable {
	driver.findElement(GUESTEMAIL).clear();
	driver.findElement(GUESTEMAIL).sendKeys("shyno2cra@gmail.com");
	Thread.sleep(3000);
	driver.findElement(GUESTCHECKOUT).click();
	Thread.sleep(3000);
}

@When("^direct to home delivery button$")
public void direct_to_home_delivery_button() throws Throwable {
	driver.findElements(HOMEDELIVERYBYINDEX).get(1).click();
	Assert.assertEquals("Delivery options", driver.findElement(By.cssSelector(".checkout-step-title")).getText());
	driver.findElement(CONTINUEBUTTON).click();
	Thread.sleep(3000);
}

@When("^select the standard delivery or next day delivery option$")
public void select_the_standard_delivery_or_next_day_delivery_option() throws Throwable {
	
}

//@When("^User direct to the address detail page$")
//public void user_direct_to_the_address_detail_page() throws Throwable {
//    
//}

@When("^enter the address details$")
public void enter_the_address_details() throws Throwable {
	Select titleDropDown=new Select(driver.findElement(TITLEDROPDOWN1));
	titleDropDown.selectByIndex(3);
	Thread.sleep(3000);
	driver.findElement(FIRSTNAME1).clear();
	driver.findElement(FIRSTNAME1).sendKeys("Shyno");
	driver.findElement(LASTNAME).clear();
	driver.findElement(LASTNAME).sendKeys("Jithosh");
	driver.findElement(ADDRESSPOSTCODE).sendKeys("MK430PG");
	driver.findElement(FINDADDRESSBUTTON1).click();
	Thread.sleep(3000);
	Select addressDropDown=new Select(driver.findElement(ADDRESSLISTDROPDOWN1));
	addressDropDown.selectByIndex(5);
	driver.findElement(BILLINGADDRESS).click();
	driver.findElement(CONTINUEBUTTON1).click();
	driver.findElement(STANDARDDELIVERYBUTTON).click();
	driver.findElement(CONTINUEBUTTON2).click();
	Thread.sleep(3000);
}

@Then("^should direct to the payment details$")
public void should_direct_to_the_payment_details() throws Throwable {
	driver.findElement(CONTINUETOPAYMENT).click();
	Thread.sleep(3000);
	driver.findElement(PAYWITHCARD).click();
	Thread.sleep(2000);
	driver.findElement(FINDADDRESSBUTTON).click();
	Thread.sleep(3000);
	Assert.assertEquals("Staying in touch", driver.findElement(By.cssSelector(".checkout-step-subtitle")).getText());
	driver.findElements(TICKTOCONTACT).get(0).click();
	driver.findElements(TERMANDCONDITION).get(1).click();
	driver.findElement(CONTINUETOPAYMENT1).click();
	System.out.println("Terms And Conditions Policy Page Popped Up");
}



//@After
//public void close() {
//	driver.close();
//	}





}
