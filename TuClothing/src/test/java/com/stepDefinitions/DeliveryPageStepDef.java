package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class DeliveryPageStepDef extends BaseClass {
	
	@Then("^should direct to click and collect option$")
	public void should_direct_to_click_and_collect_option() throws Throwable {
		deliverypage.shouldDirectToClickAndCollectOption();
		}
	@Then("^should direct to the home delivery option$")
	public void should_direct_to_the_home_delivery_option() throws Throwable {
		deliverypage.shouldDirectToTheHomeDeliveryPage();
	}
}
