package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreLocatorPageStepDef extends BaseClass {
	
	@Given("^User is on store locator page$")
	public void user_is_on_store_locator_page() throws Throwable {
		storelocatorpage.userIsOnStoreLocatorPage();
	   }
	@When("^User enter valid post code$")
	public void user_enter_valid_post_code() throws Throwable {
		storelocatorpage.functionalityEnterValidPostCode();
	}
	@Then("^should display the nearest stores$")
	public void should_display_the_nearest_stores() throws Throwable {
		storelocatorpage.shouldDisplayNearestStores();
	    }
	@When("^User enter with valid post code$")
	public void user_enter_with_valid_post_code() throws Throwable {
		storelocatorpage.userEnterPostCode();
	}
	@Then("^should show opening hours, click and collect facility of nearest stores$")
	public void should_show_opening_hours_click_and_collect_facility_of_nearest_stores() throws Throwable {
		storelocatorpage.openinghoursAndNearestStores();
	   }
	@When("^User enter a valid post code$")
	public void user_enter_a_valid_post_code() throws Throwable {
		storelocatorpage.userEnterAValidPostCode();
	}
	@Then("^User will only be able to see the stores which is available for stock clothing and click and collect$")
	public void user_will_only_be_able_to_see_the_stores_which_is_available_for_stock_clothing_and_click_and_collect() throws Throwable {
		storelocatorpage.userAbleToSeeStockClothingClickAndCollect();
	}
	@When("^User enter an invalid post code$")
	public void user_enter_an_invalid_post_code() throws Throwable {
		storelocatorpage.userEnterAnInvalidPostCode();
			}
	@Then("^should show a warning message$")
	public void should_show_a_warning_message() throws Throwable {
		storelocatorpage.shouldShowAWarningMessage();
	}

@When("^User not entering valid post code$")
public void user_not_entering_valid_post_code() throws Throwable {
	storelocatorpage.userNotEnteringPostCode();
}
@When("^click find stores$")
public void click_find_stores() throws Throwable {
	storelocatorpage.clickFindStores();
    }
@Then("^a warning message will appear$")
public void a_warning_message_will_appear() throws Throwable {
	storelocatorpage.popupOfWarningMessage();
}
@When("^not clicking the given options$")
public void not_clicking_the_given_options() throws Throwable {
	storelocatorpage.notClickingTheGivenOptions();
    }
@Then("^it should show all nearby stores$")
public void it_should_show_all_nearby_stores() throws Throwable {
	  storelocatorpage.shouldShowNearByStores();
}


}
