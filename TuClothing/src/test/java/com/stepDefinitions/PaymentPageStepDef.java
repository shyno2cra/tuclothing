package com.stepDefinitions;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class PaymentPageStepDef extends BaseClass {
	@Then("^to the payment details$")
	public void to_the_payment_details() throws Throwable {
		paymentpage.toThePaymentDetails();
	}
	@Then("^to payment details$")
	public void to_payment_details() throws Throwable {
		paymentpage.toPaymentDetails();
	}
}