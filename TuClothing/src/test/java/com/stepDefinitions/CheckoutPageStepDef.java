package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CheckoutPageStepDef extends BaseClass {
	//checkout functionality
	@Given("^User is on checkout page$")
	public void user_is_on_checkout_page() throws Throwable {
		homepage.verifyHomePage();
		checkoutpage.userIsOnCheckoutPage();
	}

@When("^User clicks on proceed to checkout button$")
public void user_clicks_on_proceed_to_checkout_button() throws Throwable {
	checkoutpage.userClicksOnProceedToCheckoutButton();
}
@When("^enters valid credentials$")
public void enters_valid_credentials() throws Throwable {
	checkoutpage.userEnterValidCredentials();
}

}
