package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BasketPageStepDef extends BaseClass {
	@Given("^User is on basket page$")
	public void user_is_on_basket_page() throws Throwable {
		homepage.verifyHomePage();
		basketpage.userIsOnBasketPage();
	}
	@When("^User clicks on the view basket and check out button$")
	public void user_clicks_on_the_view_basket_and_check_out_button() throws Throwable {
		basketpage.userClicksOnViewBasketAndCheckoutButton();
		}
	@Then("^should direct to the checkout page$")
	public void should_direct_to_the_checkout_page() throws Throwable {
		basketpage.shouldDirectToCheckoutPage();
	}
	//continue shopping facility
	@When("^User clicks on view basket and check out button$")
	public void user_clicks_on_view_basket_and_check_out_button() throws Throwable {
		basketpage.userClicksViewBasketCheckoutButton();
		}
	@Then("^User should able to do continue with shopping$")
	public void user_should_able_to_do_continue_with_shopping() throws Throwable {
	}
	@When("^User clicks on view basket and the checkout button$")
	public void user_clicks_on_view_basket_and_the_checkout_button() throws Throwable {
		basketpage.userClicksOnViewBasketAndTheCheckoutButton();
		}
	@Then("^User should able to click on the code or voucher option$")
	public void user_should_able_to_click_on_the_code_or_voucher_option() throws Throwable {
		basketpage.userAbleToClickOnTheCodeOrVoucher();
	   }
	@When("^User clicks on free click and collect$")
	public void user_clicks_on_free_click_and_collect() throws Throwable {
		basketpage.userClicksFreeClickAndCollect();
		}
	@Then("^User should get the delivery information update$")
	public void user_should_get_the_delivery_information_update() throws Throwable {
		basketpage.userGetDeliveryInformationUpdate();
	}
		

}
