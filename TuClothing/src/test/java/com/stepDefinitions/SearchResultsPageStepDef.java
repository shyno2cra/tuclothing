package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class SearchResultsPageStepDef extends BaseClass{
	
	@Then("^should appear similar results$")
	public void should_appear_similar_results() throws Throwable {
		searchresultspage.searchResultsForValidProductPage();
	   }
	@Then("^should appear dissimilar results$")
	public void should_appear_dissimilar_results() throws Throwable {
		searchresultspage.searchResultsForInvalidProductPage();
	  }
	 @Then("^should appear right category page$")
		public void should_appear_right_category_page() throws Throwable {
		 
		   }
	
	 
}
