package com.stepDefinitions;

import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;
import com.sun.jdi.Value;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginRegisterPageStepDef extends BaseClass {
//	@When("^User enter valid credentials$")
//	  public void user_enter_valid_credentials() throws Throwable {
//		loginregisterpage.validCredentials();
//		  }
	@When("^User enter valid credentials$")
	public void user_enter_valid_credentials(DataTable userDetails) throws Throwable {
	   Map<String, String>loginDetails = userDetails.asMap(String.class, String.class);
	   loginregisterpage.validCredentials(userDetails);
	}
	 @Then("^User should see the hello message with name on home page$") 
	  public void user_should_see_the_hello_message_with_name_on_home_page() throws Throwable {
		 loginregisterpage.helloMessage();
		 }
	 @When("^User enter with invalid user name and password$")
	  public void user_enter_with_invalid_user_name_and_password() throws Throwable {
		 loginregisterpage.enterInvalidUsernameAndPassword();
	 }
	 @Then("^User should see a warning message$")
	  public void user_should_see_a_warning_message() throws Throwable {
		  loginregisterpage.warningMessage();
	      }
	 @When("^User enter invalid user name and valid password$") 
	  public void user_enter_invalid_user_name_and_valid_password() throws Throwable {
		  loginregisterpage.enterInvalidUsernameAndValidPassword();
	}
	 @Then("^User should see an error message$")
	  public void user_should_see_an_error_message() throws Throwable {
		 loginregisterpage.errorMessage();
		  }
	 @When("^User enter valid user name and invalid password$")
	  public void user_enter_valid_user_name_and_invalid_password() throws Throwable {
		  loginregisterpage.enterValidUsernameAndInvalidPassword();
	       }
	 @Then("^User should see an error message due to invalid password$")
	  public void user_should_see_an_error_message_due_to_invalid_password() throws Throwable {
		 loginregisterpage.errorMessageDuetoInvalidPassword();
	  }
	 @When("^User enter without a valid user name and password$")
	 public void user_enter_without_a_valid_user_name_and_password() throws Throwable {
		loginregisterpage.enterWithoutUsernameAndPassword();
	     }
	 @Then("^User should see an error message due to missing valid data$")
	 public void user_should_see_an_error_message_due_to_missing_valid_data() throws Throwable {
		loginregisterpage.errorMessageDuetoMissingData();
	 }
	 @When("^User enter with valid user name and password$")
	 public void user_enter_with_valid_user_name_and_password() throws Throwable {
		 loginregisterpage.enterAllValidData();
	     }
	 @Then("^password should appear as asterisks or bullet signs$")
	 public void password_should_appear_as_asterisks_or_bullet_signs() throws Throwable {
		 loginregisterpage.asterisksOrBulletsigns();
	     }
	 @When("^User enter with valid credentials without entering recaptcha$")
	 public void user_enter_with_valid_credentials_without_entering_recaptcha() throws Throwable {
		 loginregisterpage.enterValidCredentialsWithoutEnteringRecaptcha();
	 }
	 @Then("^User should see a please check the recaptcha message$")
	 public void user_should_see_a_please_check_the_recaptcha_message() throws Throwable {
		 loginregisterpage.pleaseChecktheRecaptchaMessage();
	 }
	 @When("^User enter with valid user name and without password$")
	 public void user_enter_with_valid_user_name_and_without_password() throws Throwable {
		loginregisterpage.enterValidUserNameAndWithoutPassword();
	      }

	 @Then("^User should see ?forgotten Tu password page$")
	 public void user_should_see_forgotten_Tu_password_page() throws Throwable {
		loginregisterpage.forgottenYourPassword();
	     }
	// RegisterFunctionality
	 @Given("^User is on registration page$")
	 public void user_is_on_registration_page() throws Throwable {
		 homepage.verifyHomePage();
		loginregisterpage.userIsOnRegistrationPage();
	 }
	 @When("^User enter all valid information$")
	 public void user_enter_all_valid_information() throws Throwable {
		 loginregisterpage.userEnterAllValidInformations();
	 }
	 @When("^enter password and confirm password with the same characters$")
	 public void enter_password_and_confirm_password_with_the_same_characters() throws Throwable {
		 loginregisterpage.enterPasswordAndConfirmPasswordWithSameCharacters();
	      }
	 @Then("^should show assigned message$")
	 public void should_show_assigned_message() throws Throwable {
		 loginregisterpage.showAssignedMessage();
	 }
	 @When("^User enter the nectar points while registering$")
	 public void user_enter_the_nectar_points_while_registering() throws Throwable {
		loginregisterpage.userEnterNectarpointsWhileRegistering();
	     }
	 @Then("^should add on the account$")
	 public void should_add_on_the_account() throws Throwable {
		 loginregisterpage.shouldAddOnAccount();
	 }
//	 @When("^User enter all mandatory informations$")
//	 public void user_enter_all_mandatory_informations() throws Throwable {
//		 loginregisterpage.userEnterMandatoryFields();
//     }
	 @When("^User enter all mandatory informations$")
	 public void user_enter_all_mandatory_informations(DataTable userInformations) throws Throwable {
		 Map<String, String>registerDetails = userInformations.asMap(String.class, String.class);
		 loginregisterpage.userEnterMandatoryFields(userInformations);
	 }
	 @Then("^should appear as registration successful$")
	 public void should_appear_as_registration_successful() throws Throwable {
		 loginregisterpage.shouldAppearRegistrationSuccessfull();
		  }
	 @Then("^should direct to the home page$")
	 public void should_direct_to_the_home_page() throws Throwable {
	     }
	 @When("^user not enter one of the mandatory steps$")
	 public void user_not_enter_one_of_the_mandatory_steps() throws Throwable {
		 loginregisterpage.userNotEnterOneMandatoryField();
	      }
	 @Then("^a warning message should come$")
	 public void a_warning_message_should_come() throws Throwable {
		 loginregisterpage.warningMessagePopup();
	      }
	 @When("^not clicking recaptcha$")
	 public void not_clicking_recaptcha() throws Throwable {
		 loginregisterpage.notClickingRecaptcha();
	 }
	 @Then("^a warning message should appear$")
	 public void a_warning_message_should_appear() throws Throwable {
		 loginregisterpage.warningMessageAppeared();
	 }
	 @When("^without clicking the terms and conditions policy$")
	 public void without_clicking_the_terms_and_conditions_policy() throws Throwable {
		loginregisterpage.withoutClickingTermsAndConditions();
	     }
	 @Then("^User should see an error message pop up\\.$")
	 public void user_should_see_an_error_message_pop_up() throws Throwable {
		 loginregisterpage.promptMessageCame();
		 }
}
