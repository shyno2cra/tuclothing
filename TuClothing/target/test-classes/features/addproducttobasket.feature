Feature: Add Product To Basket

Scenario: Verify adding an item to the basket without login details
Given User is on product detail page
When User the select size and the quantity
And click on add basket
Then product should added in the basket

Scenario: Verify adding an item to the basket with login details
Given User is on product detail page
When User select the size and quantity
And click on add basket
Then product should add in the basket

Scenario: Verify adding multiple products to the basket
Given User is on product detail page
When User select one product, size and quantity
And click on other items 
And hit add basket
Then add basket should highlight
And products should add in basket

Scenario: Verify User adding a valid product to the basket
Given User is on product detail page
When User select needed products, size and quantity
Then add basket function should enabled



