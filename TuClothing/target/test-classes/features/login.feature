Feature: Login
#Due to Recaptcha cant reach final step of login
@smoke
Scenario: Verify the User can login with valid credentials
Given User in home page
When User click on login/register 
And User enter valid credentials
|email|jikkukalu@gmail.com|
|password|j11223344|
Then User should see the hello message with name on home page 

Scenario: Verify the User can login with invalid user name and password
Given User in home page
When User click on login/register
And User enter with invalid user name and password
Then User should see a warning message

Scenario: Verify the user can login with an invalid user name and valid password
Given User in home page
When User click on login/register
And User enter invalid user name and valid password
Then User should see an error message

Scenario: Verify the User can login with valid user name and invalid password
Given User in home page
When User click on login/register
And User enter valid user name and invalid password
Then User should see an error message due to invalid password

Scenario: Verify the User can login without a valid user name and password
Given User in home page
When User click on login/register
And User enter without a valid user name and password
Then User should see an error message due to missing valid data

Scenario: Verify the use of asterisks or bullet signs for the entry of password
Given User in home page
When User click on login/register
And User enter with valid user name and password
Then password should appear as asterisks or bullet signs
#And �show password� facility should appear

Scenario: Verify the User can login with valid credentials without entering recaptcha
Given User in home page
When User click on login/register
And User enter with valid credentials without entering recaptcha
Then User should see a please check the recaptcha message

Scenario: Verify the User can access �forgot your password� facility
Given User in home page
When User click on login/register
And User enter with valid user name and without password
Then User should see forgotten Tu password page









